# Contributing

TODO: Impreve this.

Have a look through existing
[Issues](https://gitlab.com/beraldoleal/insights4ci/-/issues) and [Merge
Requests](https://gitlab.com/beraldoleal/insights4ci/-/merge_requests) that you
could help with. If you'd like to request a feature or report a bug, please
create a [new issue](https://gitlab.com/beraldoleal/insights4ci/-/issues/new).
