.. Insights4CI documentation master file, created by
   sphinx-quickstart on Tue Feb 14 15:10:59 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Insights4CI's documentation!
=======================================

.. toctree::
   :maxdepth: 2

   Quick start <quickstart/README>
   User Guide <user/index>
   Contributor Guide <contributor/index>



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
